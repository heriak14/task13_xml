package com.epam.view;

import org.apache.logging.log4j.LogManager;

import java.io.IOException;
import java.util.Properties;

public enum  Property {
    PATH("path.properties");
    private Properties properties;

    Property(String fileName) {
        properties = new Properties();
        try {
            properties.load(getClass().getClassLoader().getResourceAsStream(fileName));
        } catch (IOException e) {
            LogManager.getLogger().error(e.getMessage());
        }
    }

    public String getProperty(String name) {
        return properties.getProperty(name);
    }
}
