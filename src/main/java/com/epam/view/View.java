package com.epam.view;

import com.epam.comparator.BankComparator;
import com.epam.parser.Parser;
import com.epam.parser.dom.DOMParser;
import com.epam.parser.sax.SAXParser;
import com.epam.parser.stax.StAXParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final Scanner SCANNER = new Scanner(System.in);
    private static final File xml = new File(Property.PATH.getProperty("xml.path"));
    private static final File xsd = new File(Property.PATH.getProperty("xsd.path"));
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Parser parser;

    public View() {
        menu = new LinkedHashMap<>();
        menu.put("1", "Parse by DOM");
        menu.put("2", "Parse by SAX");
        menu.put("3", "Parse by StAX");
        menu.put("Q", "Quit");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::parseByDOM);
        methodsMenu.put("2", this::parseBySAX);
        methodsMenu.put("3", this::parseByStAX);
        methodsMenu.put("q", this::quit);
    }

    private void showMenu(Map<String, String> menu) {
        LOGGER.trace("------------MENU------------\n");
        menu.forEach((k, v) -> LOGGER.info(k + " - " + v + "\n"));
    }

    private void parseByDOM() {
        parser = new DOMParser();
        showParseRes(parser);
    }

    private void parseBySAX() {
        parser = new SAXParser();
        showParseRes(parser);
    }

    private void parseByStAX() {
        parser = new StAXParser();
        showParseRes(parser);
    }

    private void quit() {
    }

    private void showParseRes(Parser parser) {
        try {
            parser.parse(xml, xsd).stream()
                    .sorted(new BankComparator())
                    .forEach(b -> LOGGER.info(b + "\n"));
        } catch (SAXException | ParserConfigurationException | IOException | XMLStreamException e) {
            LOGGER.error(e.getMessage());
        }
    }

    public void show() {
        String key;
        do {
            showMenu(menu);
            LOGGER.trace("Enter your choice: ");
            key = SCANNER.nextLine().toLowerCase();
            if (methodsMenu.containsKey(key)) {
                methodsMenu.get(key).print();
            } else if (!key.equals("q")) {
                LOGGER.trace("Wrong input!\n");
            }
        } while (!key.equals("q"));
    }
}
