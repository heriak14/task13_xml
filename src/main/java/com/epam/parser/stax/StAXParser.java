package com.epam.parser.stax;

import com.epam.model.Bank;
import com.epam.model.Depositor;
import com.epam.model.TimeConstraint;
import com.epam.parser.Parser;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class StAXParser implements Parser {

    @Override
    public List<Bank> parse(File xml, File xsd) throws XMLStreamException, FileNotFoundException {
        List<Bank> banks = new ArrayList<>();
        Bank bank = null;
        Depositor depositor = null;
        TimeConstraint timeConstraint = null;
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xml));
        while (xmlEventReader.hasNext()) {
            XMLEvent xmlEvent = xmlEventReader.nextEvent();
            if (xmlEvent.isStartElement()) {
                StartElement startElement = xmlEvent.asStartElement();
                String elementName = startElement.getName().getLocalPart();
                switch (elementName) {
                    case "bank":
                        xmlEvent = xmlEventReader.nextEvent();
                        bank = new Bank();
                        Attribute name = startElement.getAttributeByName(new QName("name"));
                        Attribute country = startElement.getAttributeByName(new QName("country"));
                        if (Objects.nonNull(name) && Objects.nonNull(country)) {
                            bank.setName(name.getValue());
                            bank.setCountry(country.getValue());
                        }
                        break;
                    case "type":
                        xmlEvent = xmlEventReader.nextEvent();
                        assert bank != null;
                        bank.setType(xmlEvent.asCharacters().getData());
                        break;
                    case "depositor":
                        xmlEvent = xmlEventReader.nextEvent();
                        depositor = new Depositor();
                        break;
                    case "name":
                        xmlEvent = xmlEventReader.nextEvent();
                        assert depositor != null;
                        depositor.setName(xmlEvent.asCharacters().getData());
                        break;
                    case "accountID":
                        xmlEvent = xmlEventReader.nextEvent();
                        assert depositor != null;
                        depositor.setAccountID(Integer.parseInt(xmlEvent.asCharacters().getData()));
                        break;
                    case "amount":
                        xmlEvent = xmlEventReader.nextEvent();
                        assert bank != null;
                        bank.setAmount(Integer.parseInt(xmlEvent.asCharacters().getData()));
                        break;
                    case "profitability":
                        xmlEvent = xmlEventReader.nextEvent();
                        assert bank != null;
                        bank.setProfitability(Double.parseDouble(xmlEvent.asCharacters().getData()));
                        break;
                    case "timeConstraint":
                        xmlEvent = xmlEventReader.nextEvent();
                        timeConstraint = new TimeConstraint();
                        break;
                    case "startDate":
                        xmlEvent = xmlEventReader.nextEvent();
                        assert timeConstraint != null;
                        try {
                            timeConstraint.setStartDate(new SimpleDateFormat("YYYY-MM-dd")
                                    .parse(xmlEvent.asCharacters().getData()));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        break;
                    case "endDate":
                        xmlEvent = xmlEventReader.nextEvent();
                        assert timeConstraint != null;
                        try {
                            timeConstraint.setEndDate(new SimpleDateFormat("YYYY-MM-dd")
                                    .parse(xmlEvent.asCharacters().getData()));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        assert bank != null;
                        bank.setDepositor(depositor);
                        bank.setTimeConstraint(timeConstraint);
                        break;
                    default:
                        break;
                }
            }
            if (xmlEvent.isEndElement()) {
                EndElement endElement = xmlEvent.asEndElement();
                if (endElement.getName().getLocalPart().equals("bank")) {
                    banks.add(bank);
                }
            }
        }
        return banks;
    }
}
