package com.epam.parser.dom;
import com.epam.model.Bank;
import com.epam.model.Depositor;
import com.epam.model.TimeConstraint;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

class DocReader {
    static List<Bank> readDocument(Document document) {
        List<Bank> banks = new ArrayList<>();
        NodeList nodeList = document.getElementsByTagName("bank");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Bank bank = new Bank();
            Depositor depositor;
            TimeConstraint timeConstraint;
            Node node = nodeList.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                bank.setName(element.getAttribute("name"));
                bank.setCountry(element.getAttribute("country"));
                bank.setType(element.getElementsByTagName("type").item(0).getTextContent());
                depositor = getDepositor(element.getElementsByTagName("depositor"));
                bank.setDepositor(depositor);
                bank.setAmount(Integer.parseInt(element.getElementsByTagName("amount").item(0).getTextContent()));
                bank.setProfitability(Double.parseDouble(element.getElementsByTagName("profitability")
                        .item(0).getTextContent()));
                timeConstraint = getTimeConstraint(element.getElementsByTagName("timeConstraint"));
                bank.setTimeConstraint(timeConstraint);
                banks.add(bank);
            }
        }
        return banks;
    }

    private static Depositor getDepositor(NodeList nodeList) {
        Depositor depositor = new Depositor();
        if (nodeList.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodeList.item(0);
            depositor.setName(element.getElementsByTagName("name").item(0).getTextContent());
            depositor.setAccountID(Integer.parseInt(element.getElementsByTagName("accountID")
                    .item(0).getTextContent()));
        }
        return depositor;
    }

    private static TimeConstraint getTimeConstraint(NodeList nodeList) {
        TimeConstraint timeConstraint = new TimeConstraint();
        if (nodeList.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodeList.item(0);
            try {
                timeConstraint.setStartDate(new SimpleDateFormat("YYYY-MM-dd")
                        .parse(element.getElementsByTagName("startDate")
                                .item(0).getTextContent()));
                timeConstraint.setEndDate(new SimpleDateFormat("YYYY-MM-dd")
                        .parse(element.getElementsByTagName("endDate")
                                .item(0).getTextContent()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return timeConstraint;
    }
}
