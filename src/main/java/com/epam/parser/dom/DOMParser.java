package com.epam.parser.dom;

import com.epam.model.Bank;
import com.epam.parser.Parser;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class DOMParser implements Parser {
    public List<Bank> parse(File xml, File xsd) throws IOException, SAXException, ParserConfigurationException {
        DocCreator creator = DocCreator.create(xml);
        Document doc = creator.getDocument();
        return DocReader.readDocument(doc);
    }
}
