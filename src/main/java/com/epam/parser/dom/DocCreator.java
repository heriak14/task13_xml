package com.epam.parser.dom;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

class DocCreator {
    private static DocumentBuilder documentBuilder;
    private static Document document;
    private static DocCreator creator;

    private DocCreator() {
    }

    private static void createDocBuilder() throws ParserConfigurationException {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        documentBuilder = builderFactory.newDocumentBuilder();
    }

    private static void createDoc(File xml) throws IOException, SAXException {
        document = documentBuilder.parse(xml);
    }

    static DocCreator create(File xml) throws ParserConfigurationException, IOException, SAXException {
        if (Objects.isNull(creator)) {
            creator = new DocCreator();
        }
        createDocBuilder();
        createDoc(xml);
        return creator;
    }

    Document getDocument() {
        return document;
    }
}
