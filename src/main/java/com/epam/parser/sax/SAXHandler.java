package com.epam.parser.sax;

import com.epam.model.Bank;
import com.epam.model.Depositor;
import com.epam.model.TimeConstraint;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class SAXHandler extends DefaultHandler {
    private List<Bank> banks;
    private Bank bank;
    private Depositor depositor;
    private TimeConstraint timeConstraint;

    public SAXHandler() {
        banks = new ArrayList<>();
    }

    private boolean isType;
    private boolean isDepositor;
    private boolean isName;
    private boolean isAccountID;
    private boolean isAmount;
    private boolean isProfitability;
    private boolean isTimeConstraint;
    private boolean isStartDate;
    private boolean isEndDate;

    List<Bank> getBanks() {
        return banks;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        if (qName.equalsIgnoreCase("bank")) {
            String bankName = attributes.getValue("name");
            String bankCountry = attributes.getValue("country");
            bank = new Bank();
            bank.setName(bankName);
            bank.setCountry(bankCountry);
        } else if (qName.equals("type")) {
            isType = true;
        } else if (qName.equals("depositor")) {
            isDepositor = true;
        }else if (qName.equals("name")) {
            isName = true;
        } else if (qName.equals("accountID")) {
            isAccountID = true;
        } else if (qName.equals("amount")) {
            isAmount = true;
        } else if (qName.equals("profitability")) {
            isProfitability = true;
        } else if (qName.equals("timeConstraint")) {
            isTimeConstraint = true;
        }else if (qName.equals("startDate")) {
            isStartDate = true;
        } else if (qName.equals("endDate")) {
            isEndDate = true;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        String content = new String(ch, start, length);
        if (isType){
            bank.setType(content);
            isType = false;
        } else if (isDepositor){
            depositor = new Depositor();
            isDepositor = false;
        } else if (isName){
            depositor.setName(content);
            isName = false;
        } else if (isAccountID){
            depositor.setAccountID(Integer.parseInt(content));
            isAccountID = false;
        } else if (isAmount){
            bank.setAmount(Integer.parseInt(content));
            isAmount = false;
        } else if(isProfitability){
            bank.setProfitability(Double.parseDouble(content));
            isProfitability = false;
        } else if (isTimeConstraint){
            timeConstraint = new TimeConstraint();
            isTimeConstraint = false;
        } else if (isStartDate){
            try {
                timeConstraint.setStartDate(new SimpleDateFormat("YYYY-MM-dd").parse(content));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            isStartDate = false;
        } else if (isEndDate){
            try {
                timeConstraint.setEndDate(new SimpleDateFormat("YYYY-MM-dd").parse(content));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            bank.setDepositor(depositor);
            bank.setTimeConstraint(timeConstraint);
            isEndDate = false;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName){
        if (qName.equals("bank")){
            banks.add(bank);
        }
    }
}
