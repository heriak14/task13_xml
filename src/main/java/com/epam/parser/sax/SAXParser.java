package com.epam.parser.sax;

import com.epam.model.Bank;
import com.epam.parser.Parser;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SAXParser implements Parser {
    private static SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

    @Override
    public List<Bank> parse(File xml, File xsd) throws SAXException, ParserConfigurationException, IOException {
        saxParserFactory.setSchema(SAXValidator.createSchema(xsd));
        javax.xml.parsers.SAXParser saxParser = saxParserFactory.newSAXParser();
        SAXHandler saxHandler = new SAXHandler();
        saxParser.parse(xml, saxHandler);
        return saxHandler.getBanks();
    }
}
