package com.epam.parser;

import com.epam.model.Bank;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import java.util.List;

public interface Parser {
    List<Bank> parse(File xml, File xsd) throws SAXException, ParserConfigurationException, IOException, XMLStreamException;
}
