package com.epam.model;

public class Depositor {
    private String name;
    private int accountID;

    public Depositor() {
    }

    public Depositor(String name, int accountID) {
        this.name = name;
        this.accountID = accountID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    @Override
    public String toString() {
        return "Depositor:"
                + "\n\t\tName: " + name
                + "\n\t\tAccount ID: " + accountID;
    }
}
