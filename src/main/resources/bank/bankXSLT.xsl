<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
                <link rel="stylesheet" type="text/css" href="bankCSS.css"/>
            </head>
            <body>
                <table class="bank-table">
                    <tr>
                        <th>Bank Name</th>
                        <th>Country</th>
                        <th>Type of deposit</th>
                        <th>Depositor name</th>
                        <th>Account ID</th>
                        <th>Amount</th>
                        <th>Profitability</th>
                        <th colspan="2">Duration</th>
                    </tr>
                    <xsl:for-each select="Banks/bank">
                        <tr class="banks">
                            <td>
                                <xsl:value-of select="@name" />
                            </td>
                            <td>
                                <xsl:value-of select="@country" />
                            </td>
                            <td>
                                <xsl:value-of select="type" />
                            </td>
                            <td>
                                <xsl:value-of select="depositor/name" />
                            </td>
                            <td>
                                <xsl:value-of select="depositor/accountID" />
                            </td>
                            <td>
                                <xsl:value-of select="amount" />
                            </td>
                            <td>
                                <xsl:value-of select="profitability" />
                            </td>
                            <td>
                                <xsl:value-of select="timeConstraint/startDate" />
                            </td>
                            <td>
                                <xsl:value-of select="timeConstraint/endDate" />
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>